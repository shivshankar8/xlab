/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { CssLintComponent } from './css-lint.component';

describe('CssLintComponent', () => {
  let component: CssLintComponent;
  let fixture: ComponentFixture<CssLintComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CssLintComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CssLintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
