/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { JsBinService } from './js-bin.service';

describe('JsBinService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [JsBinService]
    });
  });

  it('should ...', inject([JsBinService], (service: JsBinService) => {
    expect(service).toBeTruthy();
  }));
});
