import { Component, OnInit } from '@angular/core';
import { AirtableService } from '../airtable.service';


@Component({
  selector: 'app-student-info',
  templateUrl: './student-info.component.html',
  styleUrls: ['./student-info.component.css']
})

export class StudentInfoComponent implements OnInit {

  records;
  
  constructor(private airtableService:AirtableService) {}

  ngOnInit() {
  	this.airtableService.getRecords().subscribe( records => {
			this.records=records;
			console.log(this.records); 
    });

  	this.airtableService.getResult().subscribe( dat => console.log(dat) );
  }
}
