import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions} from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';


@Injectable()
export class JsBinService {

  private binUrl= "http://jsbin.com/hisuqigapu";	
  private lintUrl="http://jigsaw.w3.org/css-validator/validator?";
  private finalCssUrl;
  constructor(private http:Http) { }
  
  getBinCss()
    {
      return this.http.get(this.binUrl+".css").map( res => res.text());
    }
  getBinJs()
   {
   		return this.http.get(this.binUrl+".js").map(res=>res.text());
   }
}
